FROM openjdk:8-jdk
RUN wget -qO - https://packages.confluent.io/deb/4.1/archive.key | apt-key add -
RUN apt update && apt install -y software-properties-common apt-transport-https
RUN add-apt-repository "deb [arch=amd64] https://packages.confluent.io/deb/4.1 stable main"
RUN apt update && apt install -y confluent-platform-2.11
ENTRYPOINT /usr/bin/confluent start && bash
